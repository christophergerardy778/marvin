import {Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryColumn} from "typeorm";

import Label from "./Label";
import Member from "./Member";
import Process from "./Process";
import TaskLabeled from "./TaskLabeled";
import TaskMember from "./TaskMember";
import Group from "./Group";

@Entity()
export default class Task {

    public static readonly PENDING = 0;
    public static readonly COMPLETE = 1;

    @PrimaryColumn()
    id: string;

    @Column()
    title: string;

    @Column({ type: "text" })
    description: string;

    @Column({ default: 0 })
    state: number;

    @Column({ type: "date" })
    expiration: Date;

    @OneToMany(() => Process, process => process.task)
    @JoinColumn()
    process: Process[];

    @OneToMany(() => TaskLabeled, taskLabeled => taskLabeled.task)
    @JoinColumn()
    label: Label[];

    @OneToMany(() => TaskMember, taskMember => taskMember.task)
    @JoinColumn()
    member: Member[];

    @ManyToOne(() => Group)
    @JoinColumn()
    group: Group;
}