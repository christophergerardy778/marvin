import {Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryColumn} from "typeorm";
import Color from "./Color";
import User from "./User";
import Task from "./Task";

@Entity()
export default class Group {

    public static readonly PERSONAL = 0;
    public static readonly TEAM = 1;

    @PrimaryColumn()
    id: string;

    @Column({ type: "varchar", length: 100 })
    name: string;

    @Column({ default: Group.PERSONAL })
    type: number;

    @ManyToOne(() => Color)
    @JoinColumn()
    color: Color;

    @ManyToOne(() => User)
    @JoinColumn()
    user: User;

    @OneToMany(() => Task, task => task.id)
    @JoinColumn()
    tasks: Task[]

}