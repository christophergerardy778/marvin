import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export default class AccountType {

    public static readonly ADMIN_TYPE = "admin";
    public static readonly MEMBER_TYPE = "member"

    @PrimaryColumn()
    id: string;

    @Column()
    type: string;

}