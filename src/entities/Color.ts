import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export default class Color {

    public static readonly DEFAULT = "#3686FF";
    public static readonly PURPLE = "#6202EE";
    public static readonly PINK = "#EC008C";
    public static readonly GREEN = "#4CAF50";
    public static readonly GREY = "#9E9E9E";
    public static readonly YELLOW = "#FF9800";

    @PrimaryColumn()
    id: string;

    @Column()
    code: string;
}