import {Column, Entity, JoinColumn, ManyToOne, PrimaryColumn} from "typeorm";
import Group from "./Group";
import Color from "./Color";

@Entity()
export default class Label {

    @PrimaryColumn()
    id: string;

    @Column({ type: "varchar", length: 10 })
    name: string;

    @ManyToOne(() => Color)
    @JoinColumn()
    color: Color;

    @ManyToOne(() => Group)
    @JoinColumn()
    group: Group

}