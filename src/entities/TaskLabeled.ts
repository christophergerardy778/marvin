import {Entity, JoinColumn, ManyToOne, PrimaryColumn} from "typeorm";
import Label from "./Label";
import Task from "./Task";

@Entity()
export default class TaskLabeled {
    @PrimaryColumn()
    id: string;

    @ManyToOne(() => Label)
    @JoinColumn()
    label: Label;

    @ManyToOne(() => Task)
    @JoinColumn()
    task: Task
}