import {Entity, JoinColumn, ManyToOne, PrimaryColumn} from "typeorm";
import Group from "./Group";
import User from "./User";
import AccountType from "./AccountType";

@Entity()
export default class Member {

    @PrimaryColumn()
    id: string;

    @ManyToOne(() => Group)
    @JoinColumn()
    group: Group;

    @ManyToOne(() => User)
    @JoinColumn()
    user: User;

    @ManyToOne(() => AccountType)
    @JoinColumn()
    accountType: AccountType

}
