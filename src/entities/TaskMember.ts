import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from "typeorm";

import Member from "./Member";
import Task from "./Task";

@Entity()
export default class TaskMember {

    @PrimaryColumn()
    id: string;

    @ManyToOne(() => Task)
    @JoinColumn()
    task: Task

    @ManyToOne(() => Member)
    @JoinColumn()
    member: Member

}