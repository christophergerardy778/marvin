import {Column, Entity, JoinColumn, ManyToOne, PrimaryColumn} from "typeorm";
import Task from "./Task";

@Entity()
export default class Process {

    public static readonly PENDING = false;
    public static readonly COMPLETED = true;

    @PrimaryColumn()
    id: string;

    @Column({ type: "varchar", length: 15 })
    name: string;

    @Column({ type: "boolean", default: true })
    state: boolean;

    @ManyToOne(() => Task)
    @JoinColumn()
    task: Task

}