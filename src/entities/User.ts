import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export default class User {

    public static readonly DEFAULT_AVATAR = "--";

    @PrimaryColumn()
    id: string;

    @Column({ type: "varchar", length: 50 })
    names: string;

    @Column({ type: "varchar", length: 50 })
    surnames: string;

    @Column({ type: "varchar", length: 80 })
    email: string;

    @Column({ type: "varchar", length: 100 })
    password: string;

    @Column({ type: "varchar", length: 250, default: "--" })
    avatar: string;
}