import printer from "./printer";
import {getManager} from "typeorm";
import AccountType from "../entities/AccountType";
import {v4 as uuid} from "uuid";
import Color from "../entities/Color";

export default async function loadDataFactory () {
    try {

        const entityManager = getManager();

        /* ACCOUNT TYPE REGISTRATION */
        const accountTypesInDb = await entityManager.find(AccountType, {
            where: { type: AccountType.ADMIN_TYPE }
        });

        if (accountTypesInDb.length <= 0) {

            printer.Info("NO ACCOUNT TYPE REGISTERED");

            const accountTypes = [
                AccountType.ADMIN_TYPE,
                AccountType.MEMBER_TYPE
            ];

            const accountTypeEntities = accountTypes.map(accountType => {
                return entityManager.create(AccountType, {
                    id: uuid(),
                    type: accountType
                });
            });

            await entityManager.save(AccountType, accountTypeEntities);

            printer.Done("ACCOUNT TYPES REGISTRATION COMPLETE");
        }

        /* COLORS REGISTRATION */
        const colorsInDb = await entityManager.find(Color, {
           where: { code: Color.DEFAULT }
        });

        if (colorsInDb.length <= 0) {

            printer.Info("NO COLORS REGISTERED");

            const colors = [
                Color.DEFAULT,
                Color.GREEN,
                Color.GREY,
                Color.PINK,
                Color.PURPLE,
                Color.YELLOW
            ];

            const colorsEntities = colors.map(color => {
                return entityManager.create(Color, {
                    id: uuid(),
                    code: color
                });
            })

            await entityManager.save(colorsEntities);

            printer.Done("COLORS REGISTRATION COMPLETE");
        }

    } catch (e) {
        printer.Error(e);
    }
}