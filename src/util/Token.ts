import { sign, verify } from "jsonwebtoken";

import Constants from "./Constants";

export default class Token {

    public static async create (data: object): Promise<string> {
        return sign(data, Constants.TOKEN_KEY);
    }

    public static async checkToken (token: string) {
        return verify(token, Constants.TOKEN_KEY);
    }
}