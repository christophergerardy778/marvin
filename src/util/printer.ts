import 'colors';

export default class {

    public static Done(message: string) : void {
        console.log("[DONE] ".green + message)
    }

    public static Info(message: string) : void{
        console.log("[INFO] ".blue + message);
    }

    public static Error(error: string) : void{
        console.log("[ERROR] ".red + error);
    }
}