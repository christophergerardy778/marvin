export default class TextFormatter {

    public static normalizeName(name: string) : string {
        return name
            .trim()
            .toLocaleLowerCase()
            .replace(/ /g, "-");
    }

}