import {compare, genSalt, hash} from "bcryptjs";

export default class Password {

    public static async encrypt(password: string): Promise<string> {
        const salt = await  genSalt(10);
        return hash(password, salt);
    }

    public static async match(password: string, hash: string) : Promise<boolean> {
        return compare(password, hash);
    }

}