import "reflect-metadata";

import { createConnection } from "typeorm";

import Server from "./Server";

import printer from "./util/printer";
import loadDataFactory from "./util/loadDataFactory";

(async () => {
    try {
        await createConnection();
        await loadDataFactory();
        new Server()
    } catch (e) {
        printer.Error(e);
    }
})();