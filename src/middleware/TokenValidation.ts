import { Request, Response, NextFunction } from "express";

import Constants from "../util/Constants";
import printer from "../util/printer";
import Token from "../util/Token";

export default async function TokenValidation(req: Request, res: Response, next: NextFunction) : Promise<void>{
    try {

        const token = req.header(Constants.TOKEN_HEADER)

        if (token) {
            await Token.checkToken(token);
            next();
        } else {
            res.send({
               ok: false,
               message: "INICIE SESION PARA HACER ESTO"
            });
        }

    } catch (e) {
        printer.Error(e.message);

        res.send({
            ok: false,
            message: "SESSION INVALIDA"
        });
    }
}