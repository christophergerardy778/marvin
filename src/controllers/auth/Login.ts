import { Request, Response } from "express";
import { validationResult } from "express-validator";

import { getManager } from "typeorm";

import User from "../../entities/User";

import Token from "../../util/Token";
import printer from "../../util/printer";
import Password from "../../util/Password";

export default async function Login(req: Request, res: Response) : Promise<Response>{
    try {

        /* CHECK DATA FORMAT */
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.send({ errors: errors.array() });

        const entityManager = getManager();
        const user = req.body;

        /* CHECK USER EXISTS EN DB */
        const mUser = await entityManager.findOne(User, { where: { email: user.email } });

        if (!mUser) {
            return res.send({
                ok: false,
                message: "Este usuario no existe"
            });
        }

        /* CHECKING IF PASSWORD IS CORRECT */
        const passwordMatch = await Password.match(user.password, mUser.password);

        if (!passwordMatch) {
            return res.send({
                ok: false,
                message: "Contraseña incorrecta"
            })
        }

        /* USER LOGIN SUCCESS */
        const parsableUser = {
            id: mUser.id,
            names: mUser.names,
            surnames: mUser.surnames,
            email: mUser.email,
            avatar: mUser.avatar
        };

        const token = await Token.create(parsableUser);

        /* SENDING DATA RESPONSE */
        return res.send({ ok: true, token, user: parsableUser });

    } catch (e) {
        printer.Error(e);

        return res.send({
            ok: false,
            message: "Internal error app"
        });
    }
}