import { Request, Response } from "express";

import { validationResult } from "express-validator";

import { getManager } from "typeorm";
import { v4 as uuid } from "uuid";

import Token from "../../util/Token";
import printer from "../../util/printer";
import Password from "../../util/Password";
import TextFormatter from "../../util/TextFormatter";

import User from "../../entities/User";
import Group from "../../entities/Group";
import Color from "../../entities/Color";
import Member from "../../entities/Member";
import AccountType from "../../entities/AccountType";

export default async function (req: Request, res: Response) : Promise<Response> {
    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.send({ errors: errors.array() });

        const entityManager = getManager();
        const user = req.body;
        const { email, password } = user;

        const emailExists = await entityManager.findOne(User, { where: { email } });

        if (emailExists) {
            return res.send({
               ok: false,
               message: "Este email ya esta en uso"
            });
        }

        /* USER REGISTRATION */
        user.id = await uuid();
        user.password = await Password.encrypt(password);
        user.avatar = User.DEFAULT_AVATAR;

        const myUser = entityManager.create(User, user);
        await entityManager.save(User, myUser);
        delete user.password;
        const token = await Token.create(user);

        /* FIND COLOR DEFAULT */
        const color = await entityManager.findOne(Color, { where: { code: Color.DEFAULT } })

        if (color) {
            /* CREATE PERSONAL GROUP FOR USER */
            const groupId = uuid();

            const group = entityManager.create(Group, {
                id: groupId,
                name: TextFormatter.normalizeName(user.names),
                type: Group.PERSONAL,
                user: myUser,
                color: color
            });

            await entityManager.save(Group, group);

            /* GET ACCOUNT TYPE ADMIN */
            const accountTypeAdmin = await entityManager.findOne(AccountType, {
                where: { type: AccountType.ADMIN_TYPE }
            });

            /* CREATE MEMBER TYPE ADMIN */
            const groupUserRelation = entityManager.create(Member, {
                id: uuid(),
                group: group,
                user: myUser,
                accountType: accountTypeAdmin
            })

            await entityManager.save(Member, groupUserRelation);
        }

        return res.send({ ok: true, user, token });

    } catch (e) {
        printer.Error(e);

        return res.send({
            ok: false,
            message: "Internal error app"
        });
    }
}