import { Request, Response } from "express";
import { getManager } from "typeorm";

import printer from "../../util/printer";

import TaskMember from "../../entities/TaskMember";
import Member from "../../entities/Member";

export default async function getTaskHome(req: Request, res: Response) : Promise<Response>{
    try {
        const { id } = req.params;
        const entityManager = getManager();

        const member = await entityManager.findOne(Member, {
            where: { user: id }
        });

        const tasks = await entityManager.find(TaskMember, {
            where:{ member },
            take: 3,
            relations: ["task", "task.label"]
        });

        return res.send({
            ok: true,
            tasks
        });

    } catch (e) {
        printer.Error(e);

        return res.send({
           ok: false,
           error: "Internal App Error"
        });
    }
}