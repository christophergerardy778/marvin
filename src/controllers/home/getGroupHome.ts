import {Request, Response} from "express";
import printer from "../../util/printer";
import {getManager} from "typeorm";
import Member from "../../entities/Member";

export default async function getGroupHome(req: Request, res: Response) : Promise<Response> {
    try {
        /* GET USER ID FROM PARAMS */
        const userId = req.params.id;
        const entityManager = getManager();

        /* GET GROUPS BY USER LIMIT 10 */
        const userGroups = await entityManager.find(Member, {
            where: { user: userId },
            take: 10,
            relations: ["group", "group.color"]
        });

        return res.send({
            ok: true,
            groups: userGroups
        });

    } catch (e) {
        printer.Error(e);

        return res.send({
           ok: false,
           message: "Internal App Error"
        });
    }
}