import { Request, Response } from "express";
import printer from "../../util/printer";

export default async function getAllGroups(req: Request, res: Response) : Promise<Response>{
    try {
        console.log("Probando git");
        return res.send();

    } catch (e) {
        printer.Error(e);

        return res.send({
           ok: false,
           message: "Internal App error"
        });
    }
}