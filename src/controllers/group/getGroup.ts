import { Request, Response } from "express";
import printer from "../../util/printer";
import {getManager} from "typeorm";
import Group from "../../entities/Group";

export default async function getUserGroup(req: Request, res: Response) : Promise<Response> {
    try {
        const { id } = req.params;

        const group = await getManager().findOne(Group, {
            where: { id },
            relations: ["color"]
        });

        return res.send({
            ok: true,
            group
        });

    } catch (e) {
        printer.Error(e);

        return res.send({
            ok: false,
            error: "Internal App Error"
        });
    }
}