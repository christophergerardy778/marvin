import { Request, Response } from "express";
import { validationResult } from "express-validator";

import { getManager } from "typeorm";

import { v4 as uuid } from "uuid";

import printer from "../../util/printer";
import User from "../../entities/User";
import Group from "../../entities/Group";
import Color from "../../entities/Color";
import AccountType from "../../entities/AccountType";
import Member from "../../entities/Member";

export default async function createGroup(req: Request, res: Response) : Promise<Response>{
    try {

        /* CHECK DATA FORMAT */
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.send({ errors: errors.array() });

        const entityManager = getManager();
        const data = req.body;

         /* GET DATA FOR RELATION GROUP */
        const user = await entityManager.findOne(User, {
            where: { id: data.id }
        });

        const color = await entityManager.findOne(Color, {
           where: { id: data.colorId }
        });

        const accountType = await entityManager.findOne(AccountType, {
            where: { type: AccountType.ADMIN_TYPE }
        });

        /* CREATE AND SAVE GROUP */
        const group = entityManager.create(Group, {
            id: uuid(),
            user: user,
            color: color,
            name: data.name,
            type: Group.TEAM
        });

        await entityManager.save(Group, group);

        /* JOIN USER TO MEMBERS FROM NEW GROUP */

        const member = await entityManager.create(Member, {
            id: uuid(),
            user: user,
            group: group,
            accountType: accountType
        });

        await entityManager.save(Member, member);

        return res.send({
            ok: true,
            group: group
        });

    } catch (e) {
        printer.Error(e);

        return res.send({
           ok: false,
           error: "Internal app error"
        });
    }
}