import { Request, Response } from "express";
import { getManager } from "typeorm";

import { v4 as uuid } from "uuid";

import printer from "../../util/printer";

import Task from "../../entities/Task";
import Label from "../../entities/Label";
import TaskLabeled from "../../entities/TaskLabeled";
import Process from "../../entities/Process";

export default async function createTask(req: Request, res: Response) : Promise<Response> {
    try {

        const data = req.body;
        const entityManager = getManager();
        console.log(typeof req.body.test)

        const task = entityManager.create(Task, {
            id: uuid(),
            title: data.title,
            description: data.description,
            expiration: data.date,
            state: Task.PENDING
        });

        await entityManager.save(Task, task);

        const labels = await entityManager.find(Label);
        console.log(labels)
        const myLabels: any [] = [];

        const p = entityManager.create(Process, {
            id: uuid(),
            task: task,
            name: "1",
            state: Process.PENDING
        });

        const q = entityManager.create(Process, {
            id: uuid(),
            task: task,
            name: "1",
            state: Process.PENDING
        });

        await entityManager.save(Process, [p, q]);

        for (const label of labels) {

            const mLabel = entityManager.create(TaskLabeled, {
                id: uuid(),
                task: task,
                label: label
            });

            myLabels.push(myLabels);

            await entityManager.save(mLabel);
        }



        const labels2 = await entityManager.find(Task,{
            relations: ["process", "label", "label.label", "member", "member.member", "member.member.user"]
        });

        return res.send({
            ok: true,
            labels2
        });

    } catch (e) {
        printer.Error(e);

        return res.send({
           ok: false,
           message: "Internal App Error"
        });
    }
}