import { Request, Response } from "express";
import printer from "../../util/printer";
import {getManager} from "typeorm";
import Task from "../../entities/Task";

export default async function getPendingTask(req: Request, res: Response) : Promise<Response> {
    try {

        const { step, id } = req.params;
        const multiplication = Number.parseInt(step);
        const mStep = 10 * multiplication;

        const tasks = await getManager().find(Task, {
            where: { group: id, state: Task.COMPLETE },
            take: mStep
        });


        return res.send({
            ok: true,
            tasks
        });

    } catch (e) {
        printer.Error(e);

        return res.send({
            ok: false,
            message: "Internal App Error"
        })
    }
}