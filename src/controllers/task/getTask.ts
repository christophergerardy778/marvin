import { Request, Response } from "express";
import printer from "../../util/printer";
import {getManager} from "typeorm";
import Task from "../../entities/Task";

export default async function getPendingTask(req: Request, res: Response) : Promise<Response> {
    try {

        const { id } = req.params;

        const task = await getManager().find(Task, {
            where: { id },
            relations: ["process", "label", "label.label", "member"]
        });


        return res.send({
            ok: true,
            task
        });

    } catch (e) {
        printer.Error(e);

        return res.send({
            ok: false,
            message: "Internal App Error"
        })
    }
}