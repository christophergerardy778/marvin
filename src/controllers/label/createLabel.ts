import {Request, Response} from "express";
import { validationResult } from "express-validator";

import { v4 as uuid } from "uuid";

import { getManager } from "typeorm";

import Label from "../../entities/Label";

import printer from "../../util/printer";
import Color from "../../entities/Color";
import Group from "../../entities/Group";

export default async function createLabel(req: Request, res: Response) : Promise<Response>{
    try {

        /* CHECK DATA FORMAT */
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.send({ errors: errors.array() });

        /* GET MANAGER AND FORMAT DATA */
        const data = req.body;
        const entityManager = getManager();

        /* GET COLOR AND GROUP TO RELATIONSHIP*/
        const color = await entityManager.findOne(Color, {
            where: { id: data.colorId }
        });

        const group = await entityManager.findOne(Group, {
            where: { id: data.groupId }
        });

        const label = entityManager.create(Label, {
            id: uuid(),
            name: data.name,
            color: color,
            group: group
        });

        await entityManager.save(Label, label);

        return res.send({
            ok: true,
            label
        });

    } catch (e) {
        printer.Error(e);

        return res.send({
           ok: false,
           message: "Internal App Error"
        });
    }
}