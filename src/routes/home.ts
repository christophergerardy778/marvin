import router from "./router";
import TokenValidation from "../middleware/TokenValidation";
import getGroupHome from "../controllers/home/getGroupHome";
import getTaskHome from "../controllers/home/getTaskHome";


router.get("/home/:id", [TokenValidation], getGroupHome);
router.get("/home/task/:id", [TokenValidation], getTaskHome)


export default router;