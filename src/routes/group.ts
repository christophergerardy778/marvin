import { check } from "express-validator";

import router from "./router";

import TokenValidation from "../middleware/TokenValidation";

import createGroup from "../controllers/group/createGroup";
import getAllGroups from "../controllers/group/getAllGroups";
import getGroup from "../controllers/group/getGroup";


router.get("/group", TokenValidation, getAllGroups);
router.get("/group/:id", TokenValidation, getGroup);

router.post("/group", [

    TokenValidation,

    check("id", "Campo requerido")
        .notEmpty()
        .escape(),

    check("colorId", "Color requerido")
        .notEmpty()
        .escape(),

    check("name", "Campo requerido")
        .notEmpty()
        .trim()
        .escape()
        .bail()
        .isLength({ min: 6 })
        .withMessage("Nombre muy corto minimo 6 caracteres")

], createGroup);

export default router;