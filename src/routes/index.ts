import express from "express";

import auth from "./auth";
import home from "./home";
import task from "./task";
import label from "./label";
import group from "./group";
import process from "./process";
/* EXPRESS REGISTER APP */
const app = express();
/* SET REGISTERED ROUTES */
app.use(auth);
app.use(home);
app.use(task);
app.use(label);
app.use(group);
app.use(process)

export default app;