import router from "./router";
import createTask from "../controllers/task/createTask";
import TokenValidation from "../middleware/TokenValidation";
import getPendingTask from "../controllers/task/getPendingTask";
import getCompleteTasks from "../controllers/task/getCompleteTasks";
import getTask from "../controllers/task/getTask";

router.get("/task/:id", TokenValidation, getTask);
router.get("/tasks/pending/:id/:step", TokenValidation, getPendingTask);
router.get("/tasks/completed/:id/:step", TokenValidation, getCompleteTasks);

router.post("/task", [

    TokenValidation



], createTask);


export default router;