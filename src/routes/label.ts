import { check } from "express-validator";

import router from "./router";

import createLabel from "../controllers/label/createLabel";

import TokenValidation from "../middleware/TokenValidation";

router.post("/label", [

    TokenValidation,

    check("name", "Campo Requerido")
        .notEmpty()
        .withMessage("Campo vacio")
        .bail()
        .isLength({ max: 10 })
        .withMessage("Nombre muy largo maximo 10 caracteres")
        .trim()
        .escape(),

    check("colorId", "Campo Requerido")
        .notEmpty()
        .withMessage("Por favor selecciona un color")
        .bail()
        .isUUID(4)
        .withMessage("Color selecionado no valido")
        .escape(),

    check("groupId", "Campo Requerido")
        .notEmpty()
        .withMessage("Por favor selecciona un grupo")
        .bail()
        .isUUID(4)
        .withMessage("Este grupo selecionado no valido")
        .escape()

], createLabel);

export default router;