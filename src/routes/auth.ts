import { check } from "express-validator";

import router from "./router";

import Register from "../controllers/auth/Register";
import Login from "../controllers/auth/Login";

router.post("/register", [

    check("names", "Campo requerido")
        .notEmpty()
        .trim()
        .escape(),

    check("surnames", "Campo requerido")
        .notEmpty()
        .trim()
        .escape(),

    check("email", "Campo requerido")
        .notEmpty()
        .bail()
        .isEmail()
        .withMessage("Email no valido")
        .normalizeEmail(),

    check("password", "Campo requerido")
        .notEmpty()
        .bail()
        .isLength({ min: 6 })
        .withMessage("Contraseña muy corta minimo 6 caracteres")
], Register);


router.post("/login",[
    check("email", "Campo requerido")
        .notEmpty()
        .bail()
        .isEmail()
        .withMessage("Email no valido")
        .normalizeEmail(),

    check("password", "Campo requerido")
        .notEmpty()
        .escape()
], Login);

export default router;