import cors from "cors";
import clear from "clear";
import morgan from "morgan";
import express, { Application, json, urlencoded } from "express";

import router from "./routes/index";
import printer from "./util/printer";

export default class Server {

    private app: Application;
    private readonly port: number = 3000;

    constructor() {
        this.app = express();
        this.setGlobalMiddleware();
        this.setRouter();
        this.startServer();
    }

    private setGlobalMiddleware() {
        this.app.use(cors());
        this.app.use(json());
        this.app.use(morgan("dev"));
        this.app.use(urlencoded({ extended: false }));
    }

    private setRouter() {
        this.app.use(router);
    }

    private startServer() {
        this.app.listen(this.port, () => {
            clear();
            printer.Info("DATA BASE ONLINE")
            printer.Done(`SERVER ON PORT ${this.port}`)
        });
    }
}